require('dotenv').config();
const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const { interface, bytecode } = require('./compile');

const INITIAL_MESSAGE = 'Hi there!';

const provider = new  HDWalletProvider(
  process.env.MNEMONIC,
  process.env.RINKEBY_URL
);

const web3 = new Web3(provider);

const deploy = async () => {
  const accounts = await web3.eth.getAccounts();

  console.log('Attempting to deploy contract from account', accounts[0]);

  const inbox = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({ data: bytecode, arguments: [INITIAL_MESSAGE] })
    .send({from: accounts[0], gas:'1000000'});       

  console.log('Contract deployed', inbox.options.address);
}

deploy();